# gitlab-cloner

Clones all GitLab and GitLabCE projects within a given namespace

## usage
```bash
gitlab-cloner.py --help
```

### with virtualenv

```bash
$ mkvirtualenv
# echo "/home/$(whoami)/dev/gitlab-cloner/" > /home/$(whoami)/.virtualenvs/gitlab-cloner/.project
$ echo "$PATH_TO_PROJECT" >> $PATH_TO_VIRTUALENV_DIR/.project
$ pip install -r requirements.txt
$ python gitlab-cloner.py --help
```

## example
In a Makefile:

```Makefile
BIN=${HOME}/Code/mtcs.io/gitlab-cloner/gitlab-cloner.py

GITLAB_PATH=/Users/mtavella/Code
GITLAB_TOKEN=FooBar12345

default: mtcsio

mtcsio:
	@${BIN} --path=${GITLAB_PATH} --token=${GITLAB_TOKEN} --namespace=mtcs.io

.PHONY: mtcsio
```
